package com.bibao.training.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.bibao.training.model.Operation;
import com.bibao.training.model.ProfileRequest;
import com.bibao.training.model.ProfileResponse;
import com.bibao.training.service.TrainingProfileServiceImpl;

public class TrainingProfileHandler implements RequestHandler<ProfileRequest, ProfileResponse> {
	private TrainingProfileServiceImpl trainingProfileService;
	
    @Override
    public ProfileResponse handleRequest(ProfileRequest request, Context context) {
        context.getLogger().log("Request: " + request);

        trainingProfileService = TrainingProfileServiceImpl.getInstance();
        ProfileResponse response = null;
        switch (Operation.getOperation(request.getOperation())) {
        case GET: 
        	response = trainingProfileService.getProfile(request.getName());
        	break;
        case CREATE: 
        	trainingProfileService.createProfile(request.getProfile());
        	response = new ProfileResponse();
        	response.setMessage("A new training profile is created from Lambda");
        	response.setSuccessful(true);
        	break;
        case DELETE: 
        	response = trainingProfileService.deactivateProfile(request.getName());
        	break;
        case ALL: 
        	response = new ProfileResponse();
        	response.setProfiles(trainingProfileService.getAllProfiles());
        	response.setSuccessful(true);
        	break;
        default: 
        	context.getLogger().log("No available operation found!");
        	break;
        }
        return response;
    }

}
