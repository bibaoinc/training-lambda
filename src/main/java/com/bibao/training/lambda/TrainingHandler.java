package com.bibao.training.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.bibao.training.model.Operation;
import com.bibao.training.model.TrainingRequest;
import com.bibao.training.model.TrainingResponse;
import com.bibao.training.service.TrainingServiceImpl;

public class TrainingHandler implements RequestHandler<TrainingRequest, TrainingResponse> {

	@Override
	public TrainingResponse handleRequest(TrainingRequest request, Context context) {
		TrainingServiceImpl trainingService = TrainingServiceImpl.getInstance();
		TrainingResponse response = null;
		switch (Operation.getOperation(request.getOperation())) {
		case GET:
			response = trainingService.getTrainingDetails(request.getName());
			break;
		case CREATE:
			response = trainingService.addTrainingCourse(request.getDetail());
			break;
		default: break;
		}
		return response;
	}

}
