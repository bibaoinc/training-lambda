package com.bibao.training.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProfileResponse {
	private String message;
	private boolean successful;
	private TrainingProfile profile;
	private List<TrainingProfile> profiles;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isSuccessful() {
		return successful;
	}
	public void setSuccessful(boolean successful) {
		this.successful = successful;
	}
	public TrainingProfile getProfile() {
		return profile;
	}
	public void setProfile(TrainingProfile profile) {
		this.profile = profile;
	}
	public List<TrainingProfile> getProfiles() {
		return profiles;
	}
	public void setProfiles(List<TrainingProfile> profiles) {
		this.profiles = profiles;
	}
}
