package com.bibao.training.model;

public enum Operation {
	CREATE, GET, DELETE, ALL;
	
	public static Operation getOperation(String operation) {
		for (Operation op: Operation.values()) {
			if (op.name().equalsIgnoreCase(operation)) {
				return op;
			}
		}
		return null;
	}
}
