package com.bibao.training.model;

public class ProfileRequest {
	private String name;
	private TrainingProfile profile;
	private String operation;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TrainingProfile getProfile() {
		return profile;
	}
	public void setProfile(TrainingProfile profile) {
		this.profile = profile;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	@Override
	public String toString() {
		return "ProfileRequest [name=" + name + ", profile=" + profile + ", operation=" + operation + "]";
	}
	
}
