package com.bibao.training.model;


import com.bibao.training.dynamodb.entity.Address;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrainingProfile {
	private String studentId;
	private String studentName;
	private String createTimestamp;
	private Address address;
	
	public TrainingProfile() {}
	
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getCreateTimestamp() {
		return createTimestamp;
	}
	public void setCreateTimestamp(String createTimestamp) {
		this.createTimestamp = createTimestamp;
	}
	
	@Override
	public String toString() {
		return "TrainingProfile [studentId=" + studentId + ", studentName=" + studentName + ", createTimestamp="
				+ createTimestamp + ", address=" + address + "]";
	}
}
