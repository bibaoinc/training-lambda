package com.bibao.training.model;

public class TrainingRequest {
	private String operation;
	private String name;
	private TrainingDetail detail;
	
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TrainingDetail getDetail() {
		return detail;
	}
	public void setDetail(TrainingDetail detail) {
		this.detail = detail;
	}
}
