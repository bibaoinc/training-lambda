package com.bibao.training.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.bibao.training.dynamodb.dao.TrainingDaoImpl;
import com.bibao.training.dynamodb.dao.TrainingProfileDaoImpl;
import com.bibao.training.dynamodb.entity.TrainingEntity;
import com.bibao.training.dynamodb.entity.TrainingProfileEntity;
import com.bibao.training.model.TrainingDetail;
import com.bibao.training.model.TrainingResponse;

public class TrainingServiceImpl implements TrainingService {
	private static TrainingServiceImpl instance;
	
	private TrainingDaoImpl trainingDao;
	private TrainingProfileDaoImpl trainingProfileDao;
	
	private TrainingServiceImpl() {
		trainingDao = TrainingDaoImpl.getInstance();
		trainingProfileDao = TrainingProfileDaoImpl.getInstance();
	}
	
	public static TrainingServiceImpl getInstance() {
		if (instance==null) {
			instance = new TrainingServiceImpl();
		}
		return instance;
	}
	
	@Override
	public TrainingResponse addTrainingCourse(TrainingDetail trainingDetail) {
		trainingDao.enrollTraining(this.mapToTrainingEntity(trainingDetail));
		TrainingResponse response = new TrainingResponse();
		response.setMessage("Succcessfully enroll in training course " + trainingDetail.getCourse());
		response.setSuccessful(true);
		return response;
	}

	@Override
	public TrainingResponse getTrainingDetails(String name) {
		List<TrainingEntity> entities = trainingDao.getTrainings(name);
		TrainingProfileEntity profileEntity = trainingProfileDao.getTrainingProfile(name);
		TrainingResponse response = new TrainingResponse();
		if (profileEntity!=null) {
			response.setAddress(profileEntity.getAddress());
			response.setStudentId(profileEntity.getStudentId());
			response.setStudentName(profileEntity.getStudentName());
			if (CollectionUtils.isNotEmpty(entities)) {
				response.setTrainingDetails(entities.stream().map(this::mapToTrainingDetail)
						.collect(Collectors.toList()));
			}
			response.setSuccessful(true);
		} else {
			response.setMessage("No record found for student " + name);
			response.setSuccessful(false);
		}
		return response;
	}

	private TrainingEntity mapToTrainingEntity(TrainingDetail trainingDetail) {
		TrainingEntity entity = new TrainingEntity();
		entity.setPK(TrainingEntity.DEFAULT_PK);
		Date createTimestamp = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		entity.setSK(sdf.format(createTimestamp));
		entity.setCreateTimestamp(createTimestamp);
		entity.setStudentName(trainingDetail.getStudentName());
		entity.setTrainingFee(trainingDetail.getTrainingFee());
		entity.setCourse(trainingDetail.getCourse());
		return entity;
	}
	
	private TrainingDetail mapToTrainingDetail(TrainingEntity entity) {
		TrainingDetail detail = new TrainingDetail();
		detail.setCourse(entity.getCourse());
		detail.setStudentName(entity.getStudentName());
		detail.setTrainingFee(entity.getTrainingFee());
		detail.setCreateTimestamp(entity.getCreateTimestamp());
		return detail;
	}
}
