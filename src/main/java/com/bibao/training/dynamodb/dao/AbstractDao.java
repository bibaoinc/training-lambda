package com.bibao.training.dynamodb.dao;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

public class AbstractDao {
	protected static DynamoDBMapper dynamoDBMapper;
	
	public AbstractDao() {
		if (dynamoDBMapper==null) {
			AmazonDynamoDB amazonDynamoDB = AmazonDynamoDBClientBuilder.defaultClient();
			dynamoDBMapper = new DynamoDBMapper(amazonDynamoDB);
		}
	}
	
}
