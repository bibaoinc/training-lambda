package com.bibao.training.dynamodb.dao;

import java.util.List;

import com.bibao.training.dynamodb.entity.TrainingProfileEntity;

public interface TrainingProfileDao {
	public void createTrainingProfile(TrainingProfileEntity entity);
	public boolean deactivateTrainigProfile(String name);
	public TrainingProfileEntity getTrainingProfile(String name);
	public List<TrainingProfileEntity> getAllTrainingProfiles();
}
