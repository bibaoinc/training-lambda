package com.bibao.training.dynamodb.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTableMapper;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.bibao.training.dynamodb.entity.TrainingProfileEntity;

public class TrainingProfileDaoImpl extends AbstractDao implements TrainingProfileDao {
	private static TrainingProfileDaoImpl instance;
	
	private DynamoDBTableMapper<TrainingProfileEntity, String, String> trainingProfileEntityMapper;
	
	private TrainingProfileDaoImpl() {
		super();
		trainingProfileEntityMapper = dynamoDBMapper.newTableMapper(TrainingProfileEntity.class);
	}
	
	public static TrainingProfileDaoImpl getInstance() {
		if (instance==null) {
			instance = new TrainingProfileDaoImpl();
		}
		return instance;
	}
	
	@Override
	public void createTrainingProfile(TrainingProfileEntity entity) {
		trainingProfileEntityMapper.save(entity);
	}

	@Override
	public boolean deactivateTrainigProfile(String name) {
		TrainingProfileEntity entity = getTrainingProfile(name);
		if (entity==null) return false;
		entity.setActive(false);
		trainingProfileEntityMapper.save(entity);
		return true;
	}

	@Override
	public TrainingProfileEntity getTrainingProfile(String name) {
		Map<String, String> attributeNames = new HashMap<>();
		attributeNames.put("#PK", "PK");
		attributeNames.put("#studentName", "studentName");
		attributeNames.put("#active", "active");
		Map<String, AttributeValue> attributeValues = new HashMap<>();
		attributeValues.put(":PK", new AttributeValue().withS(TrainingProfileEntity.DEFAULT_PK));
		attributeValues.put(":studentName", new AttributeValue().withS(name));
		attributeValues.put(":active", new AttributeValue().withS("Y"));
		DynamoDBQueryExpression<TrainingProfileEntity> queryExpression = 
				new DynamoDBQueryExpression<TrainingProfileEntity>()
				//.withIndexName("GSI")
				.withKeyConditionExpression("#PK = :PK")
				.withFilterExpression("#studentName = :studentName and #active = :active")
				.withExpressionAttributeNames(attributeNames)
				.withExpressionAttributeValues(attributeValues)
				.withLimit(1);
		List<TrainingProfileEntity> entities = trainingProfileEntityMapper.query(queryExpression);
		if (CollectionUtils.isNotEmpty(entities)) return entities.get(0);
		else return null;
	}

	@Override
	public List<TrainingProfileEntity> getAllTrainingProfiles() {
		TrainingProfileEntity entity = new TrainingProfileEntity();
		entity.setPK(TrainingProfileEntity.DEFAULT_PK);
		DynamoDBQueryExpression<TrainingProfileEntity> queryExpression = new DynamoDBQueryExpression<>();
		queryExpression.setHashKeyValues(entity);
		return trainingProfileEntityMapper.query(queryExpression);
	}

}
