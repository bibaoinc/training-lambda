package com.bibao.training.lambda;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.amazonaws.services.lambda.runtime.Context;
import com.bibao.training.lambda.TrainingProfileHandler;
import com.bibao.training.model.ProfileRequest;
import com.bibao.training.model.ProfileResponse;

/**
 * A simple test harness for locally invoking your Lambda function handler.
 */
public class LambdaFunctionHandlerTest {

    private Context createContext() {
        TestContext ctx = new TestContext();
        ctx.setFunctionName("TrainingProfileFunction");

        return ctx;
    }

    @Test
    public void testLambdaFunctionHandler() {
        TrainingProfileHandler handler = new TrainingProfileHandler();
        Context ctx = createContext();
        ProfileRequest request = new ProfileRequest();
        request.setOperation("ALL");
        ProfileResponse response = handler.handleRequest(request, ctx);

        Assertions.assertTrue(response.isSuccessful());
        Assertions.assertTrue(response.getProfiles().size()>0);
    }
    
}
